/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.agriculture.management.ui.model.data;

/**
 *
 * @author Student
 */
public class AgricultureTypeDetails {
    private Integer id;
    private String name;
    private Double expense;
    private String useage;

    public AgricultureTypeDetails(Integer id, String name, Double expense, String useage) {
        this.id = id;
        this.name = name;
        this.expense = expense;
        this.useage = useage;
    }
  
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getExpense() {
        return expense;
    }

    public void setExpense(Double expense) {
        this.expense = expense;
    }

    public String getUseage() {
        return useage;
    }

    public void setUseage(String useage) {
        this.useage = useage;
    }
    
}
