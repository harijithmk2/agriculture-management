/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.agriculture.management.service;

import in.ac.gpckasaragod.agriculture.management.ui.AgricultureTypeDetailsForm;
import in.ac.gpckasaragod.agriculture.management.ui.model.data.AgricultureTypeDetails;
import java.util.List;

/**
 *
 * @author Student
 */
public interface AgricultureTypeService {
    public String saveAgricultureTypeDetails(String name, Double expense, String useage);
    public AgricultureTypeDetails readAgricultureTypeDetails(Integer id);
    public List<AgricultureTypeDetails>getAllAgricultureTypeDetails();
    public String updateAgricultureTypeDetails(int id, String name, Double expense, String useage);
    public String deleteAgricultureTypeDetails(Integer id);
    
}
