/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.agriculture.management.service.impl;

import in.ac.gpckasaragod.agriculture.management.service.AgricultureTypeService;
import in.ac.gpckasaragod.agriculture.management.ui.AgricultureTypeDetailsForm;
import in.ac.gpckasaragod.agriculture.management.ui.model.data.AgricultureTypeDetails;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Student
 */
public class AgricultureTypeServiceImpl extends ConnectionServiceImpl implements AgricultureTypeService {

    @Override
    public String saveAgricultureTypeDetails(String name, Double expense, String useage) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO AGRICULTURE_TYPE (NAME,EXPENSE,USEAGE) VALUES('" + name + "'," + expense + ",'" + useage + "')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Saved successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(AgricultureTypeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Save failed";

    }

    @Override
    public AgricultureTypeDetails readAgricultureTypeDetails(Integer id) {
        AgricultureTypeDetails agricultureTypeDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM AGRICULTURE_TYPE WHERE ID=" + id;
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                String name = resultSet.getString("NAME");
                Double expense = resultSet.getDouble("EXPENSE");
                String useage = resultSet.getString("USEAGE");
                agricultureTypeDetails = new AgricultureTypeDetails(id, name, expense, useage);
            }
            return agricultureTypeDetails;
        } catch (SQLException ex) {
            Logger.getLogger(AgricultureTypeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return agricultureTypeDetails;
    }

    @Override
    public String updateAgricultureTypeDetails(int id, String name, Double expense, String useage) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE AGRICULTURE_TYPE SET NAME='" + name + "',EXPENSE='" + expense + "',USEAGE='" + useage + "'";
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "update Failed";
            } else {
                return "Updated Successfully";
            }
        } catch (SQLException ex) {
            return "Update Failed";
        }
    }

    @Override
    public String deleteAgricultureTypeDetails(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM AGRICULTURE_TYPE WHERE ID =?";
            PreparedStatement statement = connection.prepareCall(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete != 1) {
                return "Delete Failed";
            } else {
                return "Deleted Successfully";
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return "Delete failed";

        }
    }


    @Override
    public List<AgricultureTypeDetails> getAllAgricultureTypeDetails() {
        List<AgricultureTypeDetails> agricultureTypeDetailses = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM AGRICULTURE_TYPE";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Integer id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                Double expense = resultSet.getDouble("EXPENSE");
                String useage = resultSet.getString("USEAGE");
                AgricultureTypeDetails agricultureTypeDetail = new AgricultureTypeDetails(id, name, expense, useage);
                agricultureTypeDetailses.add(agricultureTypeDetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AgricultureTypeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return agricultureTypeDetailses;
    }
}
